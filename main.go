package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/yroot/zapoptimizer/zap"
	"gitlab.com/yroot/zapoptimizer/zap/config"
	"gitlab.com/yroot/zapoptimizer/zap/optimizer"
)

var zapconfig config.Config

func zapHandlerFunc(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	zoneName := vars["zone"]
	zone := zapconfig.Zones[zoneName]

	rawurl := zone.OriginURL
	rawurl = strings.TrimRight(rawurl, "/")
	if rawurl == "/" {
		log.Println("Zone or OriginURL not found")
		http.NotFound(w, r)
		return
	}
	url := rawurl + r.URL.String()

	log.Println("Pull from", url)
	zo, err := zap.NewObject(url)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if webpAble(zo.GetContentType(), r) {
		zo.SetContentType("image/webp")
	}

	// if under 5 MiB
	if zo.GetContentSize() < (1024 * 1024 * 10) {
		zapOpimizer := optimizer.GetOptimizer(zo.GetContentType())
		zapOpimizer.Optimize(zo)
	} else {
		log.Println("Error: Too huge")
	}

	file, err := ioutil.ReadFile(zo.File.Name())
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", zo.GetContentType())
	w.Header().Set("Content-Length", strconv.Itoa(len(file)))
	w.Header().Set("Server", "zapoptimizer")

	w.Write(file)
}
func webpAble(contentType string, r *http.Request) bool {
	if contentType != "image/jpeg" && contentType != "image/png" && contentType != "image/tiff" {
		return false
	}
	accept := r.Header.Get("Accept")
	acceptTypes := strings.Split(accept, ",")

	for _, acceptType := range acceptTypes {
		if acceptType == "image/webp" {
			return true
		}
	}
	return false
}

func main() {

	filename, err := filepath.Abs("./zap.yaml")
	if err != nil {
		log.Fatalln(err)
	}
	c := config.ReadConfig(filename)
	zapconfig = c

	r := mux.NewRouter()

	r.Host("{zone}." + c.Host).
		HandlerFunc(zapHandlerFunc)

	log.Println("ZAP ⚡️ Optimizer")
	log.Println("Host:", c.Host)
	log.Println("Port:", c.Port)
	log.Println("Zones:")
	for name, zone := range c.Zones {
		log.Println("", name+":", "http://"+name+"."+c.Host+":"+strconv.Itoa(int(c.Port)), "->", zone.OriginURL)
	}

	server := &http.Server{
		Handler: r,
		Addr:    ":" + strconv.Itoa(int(c.Port)),
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 30 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Server started..")
	log.Fatal(server.ListenAndServe())
}
