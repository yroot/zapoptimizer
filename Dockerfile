# This file is a template, and might need editing before it works on your project.
FROM golang:alpine AS builder

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates git
WORKDIR /go/src/gitlab.com/yroot/zapoptimizer
COPY . /go/src/gitlab.com/yroot/zapoptimizer
RUN go get ./...
RUN CGO_ENABLED=0 GOOS=linux go build -v -a -installsuffix cgo -o app .


FROM alpine:latest
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
WORKDIR /data
COPY --from=builder /go/src/gitlab.com/yroot/zapoptimizer/app .
RUN apk --no-cache add jpegoptim optipng libwebp-tools 
CMD ["./app"]
