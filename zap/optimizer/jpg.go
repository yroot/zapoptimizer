package optimizer

import (
	"os/exec"

	"gitlab.com/yroot/zapoptimizer/zap"
)

// JpgOptimizer for optimizing .jpg files
type JpgOptimizer struct {
	ZapOptimizer
}

// Init initialize this JpgOptimizer
func (o JpgOptimizer) Init() error {

	return nil
}

// Optimize runs the optimizations for the zap.Object
func (o JpgOptimizer) Optimize(zo *zap.Object) error {
	cmd := exec.Command("jpegoptim", "-o", "--max=75", "-s", "--all-progressive", zo.File.Name())
	return cmd.Run()
}

// Finish Clean Up
func (o JpgOptimizer) Finish() error {
	return nil
}
