package optimizer

import "gitlab.com/yroot/zapoptimizer/zap"

// ZapOptimizer interface to handle optimiztions
type ZapOptimizer interface {
	Init() error
	Optimize(*zap.Object) error
	Finish() error
}

var zapOptimizers map[string]ZapOptimizer

// AddZapOptimizer adds a new optimize Optimizer
func AddZapOptimizer(contentType string, h ZapOptimizer) {
	zapOptimizers[contentType] = h
}

func init() {
	zapOptimizers = make(map[string]ZapOptimizer)
	AddZapOptimizer("image/png", PngOptimizer{})
	AddZapOptimizer("image/jpeg", JpgOptimizer{})
	AddZapOptimizer("image/webp", WebpOptimizer{})
}

// GetOptimizer return a ZapOptimizer for a given ConentType
func GetOptimizer(contentType string) ZapOptimizer {

	optimizer := zapOptimizers[contentType]
	if optimizer == nil {
		return SkipOptimizer{}
	}
	return optimizer
}
