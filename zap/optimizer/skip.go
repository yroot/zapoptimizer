package optimizer

import "gitlab.com/yroot/zapoptimizer/zap"

// SkipOptimizer for skip file optimisation
type SkipOptimizer struct {
	ZapOptimizer
}

// Init initialize this SkipOptimizer
func (o SkipOptimizer) Init() error {
	return nil
}

// Optimize runs the optimizations for the zap.Object
func (o SkipOptimizer) Optimize(zo *zap.Object) error {
	return nil
}

// Finish Clean Up
func (o SkipOptimizer) Finish() error {
	return nil
}
