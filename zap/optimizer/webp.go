package optimizer

import (
	"os/exec"

	"gitlab.com/yroot/zapoptimizer/zap"
)

// WebpOptimizer for optimizing .webp files
type WebpOptimizer struct {
	ZapOptimizer
}

// Init initialize this WebpOptimizer
func (o WebpOptimizer) Init() error {

	return nil
}

// Optimize runs the optimizations for the zap.Object
func (o WebpOptimizer) Optimize(zo *zap.Object) error {
	path := zo.File.Name()
	cmd := exec.Command("cwebp", "-q", "75", "-alpha_q", "100", "-m", "6", path, "-o", path)
	return cmd.Run()
}

// Finish Clean Up
func (o WebpOptimizer) Finish() error {
	return nil
}
