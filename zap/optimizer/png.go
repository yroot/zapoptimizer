package optimizer

import (
	"os/exec"

	"gitlab.com/yroot/zapoptimizer/zap"
)

// PngOptimizer for optimizing png files
type PngOptimizer struct {
	ZapOptimizer
}

// Init initialize this PngOptimizer
func (o PngOptimizer) Init() error {

	return nil
}

// Optimize runs the optimizations for the zap.Object
func (o PngOptimizer) Optimize(zo *zap.Object) error {
	path := zo.File.Name()
	cmd := exec.Command("optipng", "-o", "2", "-i", "1", "-strip", "all", path, "-o", path)
	return cmd.Run()
}

// Finish Clean Up
func (o PngOptimizer) Finish() error {
	return nil
}
