package zap

import (
	"io/ioutil"
	"net/http"
	"os"
)

type Object struct {
	File                      *os.File
	contentSize               int
	contentType               string
	contentTypeBeforeOverride string
}

func NewObject(url string) (*Object, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	file, err := ioutil.TempFile(os.TempDir(), "zap")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	_, err = file.Write(body)
	if err != nil {
		return nil, err
	}

	contentType := resp.Header.Get("Content-Type")

	return &Object{File: file, contentSize: len(body), contentType: contentType}, nil
}

func (o *Object) Close() {
	path := o.File.Name()
	o.File.Close()
	os.Remove(path)
	o.File = nil
}

func (o *Object) GetContentType() string {
	return o.contentType
}

func (o *Object) GetContentTypeBeforeOverride() string {
	return o.contentTypeBeforeOverride
}
func (o *Object) GetContentSize() int {
	return o.contentSize
}

func (o *Object) SetContentType(contentType string) {
	o.contentTypeBeforeOverride = o.contentType
	o.contentType = contentType
}
