package config

import (
	"io/ioutil"
	"log"

	yaml "gopkg.in/yaml.v2"
)

// Config for Optiproxy
type Config struct {
	Host  string          `yaml:"Host"`
	Port  uint16          `yaml:"Port"`
	Zones map[string]Zone `yaml:"Zones"`
}

// Zone defines the origin url
type Zone struct {
	OriginURL string `yaml:"OriginUrl"`
}

// ReadConfig reads a config file
func ReadConfig(filename string) Config {
	yamlFile, err := ioutil.ReadFile(filename)

	if err != nil {
		panic(err)
	}
	c := Config{}

	err = yaml.Unmarshal(yamlFile, &c)
	if err != nil {
		log.Fatalf("cannot unmarshal data: %v", err)
	}

	return c
}
